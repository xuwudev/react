import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import { Home, Login, User } from './pages';

export default function AppRoutes() {
  return (
    <Router>
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/login" element={<Login />} />
        <Route path="/user" element={<User />} />
      </Routes>
    </Router>
  )
}