export default function Header({ name }) {
  return (
    <header>
      <h1>My awesome todolist!</h1>
      <h2>My name is {name}</h2>
    </header>
  )
}
