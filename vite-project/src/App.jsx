import "./App.css"
import Header from "./components/"

function App() {
  const myName = "XuwuDev"
  return (
    <>
      <Header name={myName} />
    </>
  )
}

export default App
