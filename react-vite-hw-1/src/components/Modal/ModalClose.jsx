export default function ModalClose({ onClick }) {
  return (
    <>
      <div className="modal-close" onClick={onClick}>
        &times;
      </div>
    </>
  )
}
