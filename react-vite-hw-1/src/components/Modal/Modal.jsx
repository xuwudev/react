import React from "react"
import ModalWrapper from "./ModalWrapper"
import "./Modal.scss"

export default function Modal({ children, onClose }) {
  const handleClose = (event) => {
    if (event.target === event.currentTarget) {
      onClose()
    }
  }

  return (
    <ModalWrapper>
      <div className="modal-container" onClick={handleClose}>
        <div className="modal">
          <div className="modal-close" onClick={onClose}>
            <svg
              width="12"
              height="12"
              viewBox="0 0 22 22"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                d="M18 4L4 18M18 18L4 4.00001"
                stroke="#3C4242"
                strokeWidth="1.5"
                strokeLinecap="round"
              />
            </svg>
          </div>
          {children}
        </div>
      </div>
    </ModalWrapper>
  )
}
