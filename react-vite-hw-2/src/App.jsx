import { useEffect, useState } from "react"
import PropTypes from "prop-types"
import ProductList from "./components/ProductAppearence/ProductList/ProductList"
import Modal from "./components/Modal/Modal"

import "./App.css"

export default function App() {
  const [products, setProducts] = useState([])
  const [cartItems, setCartItems] = useState(
    JSON.parse(localStorage.getItem("cartItems")) || []
  )
  const [favoriteItems, setFavoriteItems] = useState(
    JSON.parse(localStorage.getItem("favoriteItems")) || []
  )
  const [showCartModal, setShowCartModal] = useState(false)

  useEffect(() => {
    const fetchProducts = async () => {
      try {
        const response = await fetch("/ProductBase.json")
        if (!response.ok) {
          throw new Error("Failed to fetch products")
        }
        const data = await response.json()
        setProducts(data)
      } catch (error) {
        console.error("Error fetching products:", error)
      }
    }

    fetchProducts()
  }, [])

  const handleAddToCart = (product) => {
    const indexItem = cartItems.findIndex((item) => item.id === product.id)

    if (indexItem === -1) {
      const newItem = {
        id: product.id,
        title: product.title,
        price: product.price,
        image: product.image,
        quantity: 1,
      }

      const inCartItems = [...cartItems, newItem]
      setCartItems(inCartItems)
      localStorage.setItem("cartItems", JSON.stringify(inCartItems))
    } else {
      const updatedItem = {
        ...cartItems[indexItem],
        quantity: cartItems[indexItem].quantity + 1,
      }
      const updatedCartItems = [...cartItems]
      updatedCartItems.splice(indexItem, 1, updatedItem)

      setCartItems(updatedCartItems)
      localStorage.setItem("cartItems", JSON.stringify(updatedCartItems))
    }
  }

  const toggleCartModal = () => {
    setShowCartModal(!showCartModal)
  }

  const handleAddToFavorites = (product) => {
    const indexItem = favoriteItems.findIndex((item) => item.id === product.id)

    if (indexItem === -1) {
      const newItem = {
        id: product.id,
        title: product.title,
        price: product.price,
        image: product.image,
      }

      const inFavoriteItems = [...favoriteItems, newItem]
      setFavoriteItems(inFavoriteItems)
      localStorage.setItem("favoriteItems", JSON.stringify(inFavoriteItems))
    } else {
      const updatedFavoriteItems = favoriteItems.filter(
        (item) => item.id !== product.id
      )
      setFavoriteItems(updatedFavoriteItems)
      localStorage.setItem(
        "favoriteItems",
        JSON.stringify(updatedFavoriteItems)
      )
    }
  }

  const handleOutsideCartClick = (event) => {
    if (!event.target.closest(".modal")) {
      toggleCartModal()
    }
  }

  const cartItemTotal = cartItems.reduce(
    (total, item) => total + item.quantity,
    0
  )
  const favoriteItemTotal = favoriteItems.length

  return (
    <div className="app">
      <header className="header">
        <div className="icons-wrapper">
          <div className="cart-shopping-wrapper">
            <svg
              xmlns="http://www.w3.org/2000/svg"
              width="24"
              height="24"
              viewBox="0 0 24 24"
              fill="none"
              stroke="currentColor"
              strokeWidth="1.5"
              strokeLinecap="round"
              strokeLinejoin="round"
              className="feather feather-shopping-cart"
            >
              <circle cx="9" cy="21" r="1" />
              <circle cx="20" cy="21" r="1" />
              <path d="M1 1h4l2.68 13.39a2 2 0 0 0 2 1.61h9.72a2 2 0 0 0 2-1.61L23 6H6" />
            </svg>
            <span>{cartItemTotal}</span>
          </div>
          <div className="heart-wrapper">
            <svg
              fill="white"
              height="25px"
              width="25px"
              version="1.1"
              id="Capa_1"
              xmlns="http://www.w3.org/2000/svg"
              xmlnsXlink="http://www.w3.org/1999/xlink"
              viewBox="0 0 471.701 471.701"
              xmlSpace="preserve"
            >
              <g id="SVGRepo_bgCarrier" strokeWidth="0"></g>
              <g
                id="SVGRepo_tracerCarrier"
                strokeLinecap="round"
                strokeLinejoin="round"
              ></g>
              <g id="SVGRepo_iconCarrier">
                {" "}
                <g>
                  {" "}
                  <path d="M433.601,67.001c-24.7-24.7-57.4-38.2-92.3-38.2s-67.7,13.6-92.4,38.3l-12.9,12.9l-13.1-13.1 c-24.7-24.7-57.6-38.4-92.5-38.4c-34.8,0-67.6,13.6-92.2,38.2c-24.7,24.7-38.3,57.5-38.2,92.4c0,34.9,13.7,67.6,38.4,92.3 l187.8,187.8c2.6,2.6,6.1,4,9.5,4c3.4,0,6.9-1.3,9.5-3.9l188.2-187.5c24.7-24.7,38.3-57.5,38.3-92.4 C471.801,124.501,458.301,91.701,433.601,67.001z M414.401,232.701l-178.7,178l-178.3-178.3c-19.6-19.6-30.4-45.6-30.4-73.3 s10.7-53.7,30.3-73.2c19.5-19.5,45.5-30.3,73.1-30.3c27.7,0,53.8,10.8,73.4,30.4l22.6,22.6c5.3,5.3,13.8,5.3,19.1,0l22.4-22.4 c19.6-19.6,45.7-30.4,73.3-30.4c27.6,0,53.6,10.8,73.2,30.3c19.6,19.6,30.3,45.6,30.3,73.3 C444.801,187.101,434.001,213.101,414.401,232.701z"></path>{" "}
                </g>{" "}
              </g>
            </svg>
            <span>{favoriteItemTotal}</span>
          </div>
        </div>
      </header>

      <ProductList
        products={products}
        cartItems={cartItems}
        favoriteItems={favoriteItems}
        AddToCart={handleAddToCart}
        AddToFavorites={handleAddToFavorites}
        showCartModal={toggleCartModal}
      />
      {showCartModal && (
        <Modal
          closeModal={toggleCartModal}
          handleOutsideClick={handleOutsideCartClick}
        >
          <h2 className="modal-title">Product Added to Your Cart</h2>
        </Modal>
      )}
    </div>
  )
}

App.propTypes = {
  products: PropTypes.array,
  cartItems: PropTypes.array,
  favoriteItems: PropTypes.array,
  AddToCart: PropTypes.func,
  AddToFavorites: PropTypes.func,
  showCartModal: PropTypes.func,
  closeModal: PropTypes.bool,
  handleOutsideClick: PropTypes.func,
  handleAddToCart: PropTypes.func,
}
