import React from "react"
import "./Modal.scss"
import PropTypes from "prop-types"

export default function Modal({ closeModal, handleOutsideClick, children }) {
  return (
    <div
      className="modal-container"
      onClick={(event) => handleOutsideClick(event)}
    >
      <div className="modal">
        <div className="modal-close" onClick={closeModal}>
          <svg
            width="16"
            height="16"
            viewBox="0 0 22 22"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path
              d="M18 4L4 18M18 18L4 4.00001"
              stroke="#3C4242"
              strokeWidth="1.5"
              strokeLinecap="round"
            />
          </svg>
        </div>
        {children}
      </div>
    </div>
  )
}

Modal.propTypes = {
  closeModal: PropTypes.func,
  handleOutsideClick: PropTypes.func,
  children: PropTypes.any,
}
