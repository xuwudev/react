import PropTypes from "prop-types"
import Button from "../../Button/Button"

import "./ProductCard.scss"

export default function ProductCard(props) {
  const handleClickAddToCart = () => {
    props.showCartModal(props.product)
    props.AddToCart(props.product)
  }

  const handleClickAddToFavorites = () => {
    props.AddToFavorites(props.product)
  }

  const handleItemCartClose = () => {
    props.showCartModal(props.product)
  }

  const { title, price, image, sku } = props.product
  const { inCart, inFavorites } = props

  return (
    <>
      <div className="product-card">
        <img width="280px" height="220px" src={image} alt={title} />

        <h3 className="product-card-title ">{title}</h3>
        <p className="product-price">{price} $</p>

        <div className="buttons-wrapper">
          {!inCart && (
            <Button className="add-to-cart" onClick={handleClickAddToCart}>
              Add to Cart!
            </Button>
          )}

          {inCart && (
            <Button className="remove-cart" onClick={handleItemCartClose}>
              In cart!
            </Button>
          )}

          {!inFavorites && (
            <Button
              className="add-favorite"
              onClick={handleClickAddToFavorites}
            >
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="25"
                height="25"
                viewBox="0 0 24 24"
                fill="none"
                stroke="white"
                strokeWidth="1.5"
                strokeLinecap="round"
                strokeLinejoin="round"
                className="feather feather-heart"
              >
                <path d="M20.84 4.61a5.5 5.5 0 0 0-7.78 0L12 5.67l-1.06-1.06a5.5 5.5 0 0 0-7.78 7.78l1.06 1.06L12 21.23l7.78-7.78 1.06-1.06a5.5 5.5 0 0 0 0-7.78z" />
              </svg>
            </Button>
          )}

          {inFavorites && (
            <Button
              className="add-favorite"
              onClick={handleClickAddToFavorites}
            >
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="25"
                height="25"
                viewBox="0 0 24 24"
                fill="pink"
                stroke="black"
                strokeWidth="1.5"
                strokeLinecap="round"
                strokeLinejoin="round"
                className="feather feather-heart"
              >
                <path d="M20.84 4.61a5.5 5.5 0 0 0-7.78 0L12 5.67l-1.06-1.06a5.5 5.5 0 0 0-7.78 7.78l1.06 1.06L12 21.23l7.78-7.78 1.06-1.06a5.5 5.5 0 0 0 0-7.78z" />
              </svg>
            </Button>
          )}
        </div>
      </div>
    </>
  )
}

ProductCard.propTypes = {
  product: PropTypes.shape({
    title: PropTypes.string,
    price: PropTypes.number,
    image: PropTypes.string,
  }),
  inCart: PropTypes.bool,
  inFavorites: PropTypes.bool,
  AddToCart: PropTypes.func,
  AddToFavorites: PropTypes.func,
  showCartModal: PropTypes.func,
}
