import React from "react"
import { productList } from "../../../public/productList"
import { Product } from "./Product"
import "./shop.scss"

export const Shop = () => {
  return (
    <div className="shop">
      <div className="shopTitle">
        <h1>SxShop</h1>
      </div>
      <div className="shop">
        {productList.map((product) => (
          <Product data={product} />
        ))}
      </div>
    </div>
  )
}
