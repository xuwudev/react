import React, { useContext } from "react"
import { ShopContext } from "../../context/ShopContext"
import "./product.scss"
export const Product = (props) => {
  const { id, title, price, image } = props.data
  const { addToCart, cartItems } = useContext(ShopContext)
  const cartItemAmount = cartItems[id]

  return (
    <div className="product">
      <img width="200px" height="200px" src={image} />
      <p>
        <b>{title}</b>
      </p>
      <p>${price}</p>
      <button className="addToCartBtn" onClick={() => addToCart(id)}>
        Add To Cart {cartItemAmount > 0 && <> ({cartItemAmount})</>}
      </button>
    </div>
  )
}
