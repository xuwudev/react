export const productList = [
  {
    id: 1,
    title: "Сукня вечірня",
    price: 149.99,
    image:
      "https://images.unsplash.com/flagged/photo-1585052201332-b8c0ce30972f?q=80&w=1935&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D",
    sku: "123456789",
    color: "Зелена",
  },
  {
    id: 2,
    title: "Костюм чоловічий",
    price: 199.99,
    image:
      "https://www.boggi.com/on/demandware.static/-/Sites-BoggiCatalog/default/dw0db93e81/images/hi-res/BO23P019901_1.jpeg",
    sku: "987654321",
    color: "Синій",
  },
  {
    id: 3,
    title: "Спортивні штани",
    price: 59.99,
    image:
      "https://static2.issaplus.com/wa-data/public/shop/products/78/44/74478/images/168799/168799.1200x0.jpg",
    sku: "2468101214",
    color: "Чорний",
  },
  {
    id: 4,
    title: "Футболка графічна",
    price: 29.99,
    image: "https://img.klubok.com/img/used/2023/08/07/34871/34871171_2.jpg",
    sku: "1357924680",
    color: "Біла",
  },
  {
    id: 5,
    title: "Світшот з капюшоном",
    price: 39.99,
    image:
      "https://static.reserved.com/media/catalog/product/cache/1200/a4e40ebdc3e371adff845072e1c73f37/1/1/1176W-09M-010-1-727339_5.jpg",
    sku: "111222333",
    color: "Сірий",
  },
  {
    id: 6,
    title: "Спортивні кросівки",
    price: 79.99,
    image:
      "https://shoes-poland.com.ua/image/cache/catalog/men/5341/5341-02-1000x1000.jpg",
    sku: "444555666",
    color: "Червоно-чорний",
  },
  {
    id: 7,
    title: "Капелюх панама",
    price: 19.99,
    image:
      "https://media2.vsisvoi.ua/uploads/catalog/products/2022/03/31/293815/gallery/ea89b1e9464d88de614b95dde5de7af9.jpg",
    sku: "777888999",
    color: "Коричневий",
  },
  {
    id: 8,
    title: "Рюкзак шкіряний",
    price: 129.99,
    image:
      "https://blanknote.ua/image/cache/catalog/leather/bags/bn-bag-13/bn-bag-13-onyx/front(4)-min-729x1050.jpg",
    sku: "1010101010",
    color: "Чорний",
  },
  {
    id: 9,
    title: "Окуляри сонцезахисні",
    price: 49.99,
    image:
      "https://opticaluxor.ua/app/uploads/public/629/496/70c/62949670c69e3106470778.jpg",
    sku: "1212121212",
    color: "Чорний",
  },
  {
    id: 10,
    title: "Годинник наручний",
    price: 99.99,
    image:
      "https://chasik.com.ua/image/cache/catalog/easyphoto/4274/zhenskie-silikonovye-chasy-zheltye-2-1000x1000.jpg",
    sku: "1313131313",
    color: "Золотий",
  },
]
